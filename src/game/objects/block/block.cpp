#include "block.h"

using namespace sokoban;

block::block(Texture2D pad, float posX, float posY, short id) {
	block::init(pad, posX, posY,id);
}
block::~block() {	
	
}
void block::init(Texture2D pad, float posX, float posY,short id) {
	ID = id;
	active = true;
	_block = pad;	
	blockPos = { posX,posY};
	color = RAYWHITE;
}

void block::update() {
	
}
void block::draw() {	
#if _DEBUG
	DrawRectangleLines(static_cast<int>(blockPos.x), static_cast<int>(blockPos.y), static_cast<int>(_block.width), static_cast<int>(_block.height), RED);
#endif	
	DrawTexture(_block, static_cast<int>(blockPos.x), static_cast<int>(blockPos.y), color);
}

bool block::getActive() {
	return active;
}
short  block::getID() {
	return ID;
}
Texture2D block::getTexture() {
	return _block;
}
void block::setColor(Color _color) {
	color = _color;
}
void block::setActive(bool _active) {
	active = _active;
}
void block::setID(short id) {
	ID = id;
}
void block::setTexture(Texture2D pad) {
	_block = pad;
}