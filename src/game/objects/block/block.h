#ifndef BLOCK_H
#define BLOCK_H

#include "raylib.h"
#include "game/globales.h"

namespace sokoban {

	class block {
	private:
		Texture2D _block;				
		short ID;
		bool active;
		Vector2 blockPos;
		Color color;

	public:		
		block(Texture2D pad, float posX, float posY,short id);
		~block();
		void init(Texture2D pad, float posX, float posY,short id);
		void update();
		void draw();		
		bool getActive();
		short getID();
		Texture2D getTexture();	
		void setColor(Color _color);
		void setActive(bool _active);			
		void setID(short id);
		void setTexture(Texture2D pad);
	};
}
#endif

