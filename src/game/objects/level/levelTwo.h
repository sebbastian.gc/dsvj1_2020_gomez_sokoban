#ifndef LEVELTWO_H
#define LEVELTWO_H

#include "raylib.h"
#include "level.h"

namespace sokoban {

	class levelTwo : public level {
	public:
		levelTwo(bool _active);
		~levelTwo();
		void levelDesign();
		void updatePoint();
		void isWin();
		void update();
	};
}
#endif
