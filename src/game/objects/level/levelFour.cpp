#include "levelFour.h"

using namespace sokoban;

levelFour::levelFour(bool _active) :level(_active) {

	levelDesign();
}
levelFour::~levelFour() {

}
void levelFour::levelDesign() {
	for (short i = 0; i < size; i++) {
		for (short j = 0; j < size; j++) {
			if (i == 0 || j == 0 || j == size - 1 || i == size - 1 || i == size - 2 || i == size - 3) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::BLOCK)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::BLOCK));
			}
			else {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::FLOOR)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::FLOOR));
			}
			if (i == 2 && j == 2 || i == 2 && j == 4 || i == 1 && j == 4 || i == 4 && j == 3 || i == 4 && j == 5 || i == 4 && j == 6 || i == 4 && j == 7 || i == 5 && j == 7) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::BLOCK)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::BLOCK));
			}
			if (i == 6 && j == 4) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::FLOOR)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::FLOOR));
			}
			if (i == 3 && j == 4) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::PJ)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::PJ));
			}
			if (i == 4 && j == 4 || i == 4 && j == 6 || i == 2 && j == 1 || i == 6 && j == 3) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::BOX)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::BOX));

			}
			if (i == 6 && j == 1 || i == 6 && j == 2 || i == 4 && j == 1 || i == 4 && j == 2) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::END)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::END));
				blocks[i][j]->setColor(RED);
			}
		}
	}
}

void levelFour::updatePoint() {
	for (short i = 0; i < size; i++) {
		for (short j = 0; j < size; j++) {
			if (i == 6 && j == 1 || i == 6 && j == 2 || i == 4 && j == 1 || i == 4 && j == 2) {
				if (blocks[i][j]->getID() == static_cast<int>(OBJ::FLOOR)) {
					blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::END)]);
					blocks[i][j]->setID(static_cast<int>(OBJ::END));
				}
			}
		}
	}
}

void levelFour::isWin() {
	if (blocks[6][1]->getID() == static_cast<int>(OBJ::BOX) && blocks[6][2]->getID() == static_cast<int>(OBJ::BOX) && blocks[4][1]->getID() == static_cast<int>(OBJ::BOX) && blocks[4][2]->getID() == static_cast<int>(OBJ::BOX)) {
		win = true;
	}
}

void levelFour::update() {
	if (resetLevel) {
		levelDesign();
		resetLevel = false;
	}
	moveCharacter();
	updatePoint();
	isWin();
}
