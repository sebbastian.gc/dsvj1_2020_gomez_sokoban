#include "levelThree.h"

using namespace sokoban;

levelThree::levelThree(bool _active) :level(_active) {

	levelDesign();
}
levelThree::~levelThree() {

}
void levelThree::levelDesign() {
	for (short i = 0; i < size; i++) {
		for (short j = 0; j < size; j++) {
			if (i == 0 || j == 0 || j == size - 1 || i == size - 1 || i == size - 2 || j == size - 2 || i == 1 || j == 1) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::BLOCK)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::BLOCK));
			}
			else {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::FLOOR)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::FLOOR));
			}
			if (i == 4 && j == 2 || i == 5 && j == 2 || i == 3 && j == 4 || i == 4 && j == 4 || i == 6 && j == 5) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::BLOCK)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::BLOCK));
			}
			if (i == 2 && j == 5 || i == 3 && j == 5 || i == 4 && j == 5 || i == 2 && j == 6 || i == 3 && j == 6 || i == 4 && j == 6 || i == 2 && j == 4) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::BLOCK)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::BLOCK));
			}
			if (i == 1 && j == 3|| i == 1 && j == 2|| i == 1 && j == 1|| i ==2 && j == 1|| i == 3 && j == 1||i == 6 && j == 7|| i == 7 && j == 2 || i == 7 && j == 3 || i == 7 && j == 4) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::FLOOR)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::FLOOR));
			}
			if (i == 2 && j == 3) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::PJ)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::PJ));
			}
			if (i == 2 && j == 2 || i == 3 && j == 2 || i == 3 && j == 3) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::BOX)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::BOX));

			}
			if (i == 3 && j == 7 || i == 4 && j == 7 || i == 5 && j == 7) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::END)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::END));
				blocks[i][j]->setColor(RED);
			}
		}
	}
}

void levelThree::updatePoint() {
	for (short i = 0; i < size; i++) {
		for (short j = 0; j < size; j++) {
			if (i == 3 && j == 7 || i == 4 && j == 7 || i == 5 && j == 7) {
				if (blocks[i][j]->getID() == static_cast<int>(OBJ::FLOOR)) {
					blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::END)]);
					blocks[i][j]->setID(static_cast<int>(OBJ::END));
				}
			}
		}
	}
}

void levelThree::isWin() {
	if (blocks[3][7]->getID() == static_cast<int>(OBJ::BOX) && blocks[4][7]->getID() == static_cast<int>(OBJ::BOX) && blocks[5][7]->getID() == static_cast<int>(OBJ::BOX)) {
		win = true;
	}
}

void levelThree::update() {
	if (resetLevel) {
		levelDesign();
		resetLevel = false;
	}
	moveCharacter();
	updatePoint();
	isWin();
}
