#include "level.h"
#include "game/input/input.h"

using namespace sokoban;
using namespace input;

level::level(bool _active) {
	level::init(_active);
}
level::~level() {
	for (short i = 0; i < sizeObj; i++)	{
		UnloadTexture(objects[i]);
	}
	for (short i = 0; i < size; i++) {
		for (short j = 0; j < size; j++) {
			delete blocks[i][j];
			blocks[i][j] = NULL;
		}
	}
	UnloadSound(fxEnd);
}
void level::init(bool _active) {	
	objects[0]= LoadTexture("res/assets/bloque.png");
	objects[1] = LoadTexture("res/assets/piso.png");
	objects[2] = LoadTexture("res/assets/caja.png");
	objects[3] = LoadTexture("res/assets/final.png");
	objects[4] = LoadTexture("res/assets/up.png");
	objects[5] = LoadTexture("res/assets/down.png");
	objects[6] = LoadTexture("res/assets/left.png");
	objects[7] = LoadTexture("res/assets/right.png");

	fxEnd = LoadSound("res/fx/end.mp3");

	for (short i = 0; i < size; i++) {
		for (short j = 0; j < size; j++) {
			blocks[i][j] = NULL;
		}
	}
	spawnBlocks();	
	active = _active;
}

void level::draw() {
	for (short i = 0; i < size; i++) {
		for (short j = 0; j < size; j++) {
			blocks[i][j]->draw();
		}
	}
}

void level::spawnBlocks() {
	short x = 50;
	short y = 60;

	for (short i = 0; i < size; i++) {
		for (short j = 0; j < size; j++) {			
			if (j == 0) {
				blocks[i][j] = new block(objects[static_cast<int>(OBJ::BLOCK)], static_cast<float>(x), static_cast<float>(y + (objects[static_cast<int>(OBJ::BLOCK)].height * i)), static_cast<int>(OBJ::BLOCK));
			}
			else blocks[i][j] = new block(objects[static_cast<int>(OBJ::BLOCK)], static_cast<float>(x + (objects[static_cast<int>(OBJ::BLOCK)].width * j)), static_cast<float>(y + (objects[static_cast<int>(OBJ::BLOCK)].height * i)), static_cast<int>(OBJ::BLOCK));
		}
	}
}

void level::moveCharacter() {
	for (short i = 0; i < size; i++) {
		for (short j = 0; j < size; j++) {
			if (blocks[i][j]->getID() == static_cast<int>(OBJ::PJ)) {				
				if (up && (blocks[i - 1][j]->getID() == static_cast<int>(OBJ::END) ||blocks[i-1][j]->getID() == static_cast<int>(OBJ::FLOOR)|| (blocks[i - 1][j]->getID() == static_cast<int>(OBJ::BOX)&& blocks[i - 2][j]->getID() == static_cast<int>(OBJ::FLOOR))|| (blocks[i - 1][j]->getID() == static_cast<int>(OBJ::BOX) && blocks[i - 2][j]->getID() == static_cast<int>(OBJ::END)))) {
					if (blocks[i - 1][j]->getID() == static_cast<int>(OBJ::BOX)) {
						if (blocks[i - 2][j]->getID() == static_cast<int>(OBJ::END)) { PlaySound(fxEnd); }
						blocks[i - 2][j]->setTexture(objects[static_cast<int>(OBJ::BOX)]);
						blocks[i - 2][j]->setID(static_cast<int>(OBJ::BOX));
					}
					blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::FLOOR)]);
					blocks[i][j]->setID(static_cast<int>(OBJ::FLOOR));
					blocks[i-1][j]->setTexture(objects[static_cast<int>(OBJ::PJ)]);
					blocks[i-1][j]->setID(static_cast<int>(OBJ::PJ));					
					up = false;
				}
				else if (down && (blocks[i + 1][j]->getID() == static_cast<int>(OBJ::END) || blocks[i+1][j]->getID() == static_cast<int>(OBJ::FLOOR) || (blocks[i + 1][j]->getID() == static_cast<int>(OBJ::BOX) && blocks[i + 2][j]->getID() == static_cast<int>(OBJ::FLOOR)) || (blocks[i + 1][j]->getID() == static_cast<int>(OBJ::BOX) && blocks[i + 2][j]->getID() == static_cast<int>(OBJ::END)))) {
					if (blocks[i +1][j]->getID() == static_cast<int>(OBJ::BOX)) {
						if (blocks[i + 2][j]->getID() == static_cast<int>(OBJ::END)) { PlaySound(fxEnd); }
						blocks[i + 2][j]->setTexture(objects[static_cast<int>(OBJ::BOX)]);
						blocks[i + 2][j]->setID(static_cast<int>(OBJ::BOX));
					}
					blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::FLOOR)]);
					blocks[i][j]->setID(static_cast<int>(OBJ::FLOOR));
					blocks[i + 1][j]->setTexture(objects[static_cast<int>(OBJ::PJ)+1]);
					blocks[i + 1][j]->setID(static_cast<int>(OBJ::PJ));
					down = false;
				}
				else if (left && (blocks[i][j-1]->getID() == static_cast<int>(OBJ::END) || blocks[i][j-1]->getID() == static_cast<int>(OBJ::FLOOR) || (blocks[i][j-1]->getID() == static_cast<int>(OBJ::BOX) && blocks[i][j-2]->getID() == static_cast<int>(OBJ::FLOOR)) || (blocks[i][j-1]->getID() == static_cast<int>(OBJ::BOX) && blocks[i][j-2]->getID() == static_cast<int>(OBJ::END)))) {
					if (blocks[i ][j-1]->getID() == static_cast<int>(OBJ::BOX)) {
						if (blocks[i][j-2]->getID() == static_cast<int>(OBJ::END)) { PlaySound(fxEnd); }
						blocks[i][j-2]->setTexture(objects[static_cast<int>(OBJ::BOX)]);
						blocks[i][j-2]->setID(static_cast<int>(OBJ::BOX));
					}
					blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::FLOOR)]);
					blocks[i][j]->setID(static_cast<int>(OBJ::FLOOR));
					blocks[i][j-1]->setTexture(objects[static_cast<int>(OBJ::PJ)+2]);
					blocks[i][j-1]->setID(static_cast<int>(OBJ::PJ));
					left = false;
				}
				else if (right && (blocks[i][j+1]->getID() == static_cast<int>(OBJ::END) || blocks[i][j+1]->getID() == static_cast<int>(OBJ::FLOOR) || (blocks[i][j+1]->getID() == static_cast<int>(OBJ::BOX) && blocks[i][j+2]->getID() == static_cast<int>(OBJ::FLOOR)) || (blocks[i ][j+1]->getID() == static_cast<int>(OBJ::BOX) && blocks[i][j+2]->getID() == static_cast<int>(OBJ::END)))) {
					if (blocks[i][j + 1]->getID() == static_cast<int>(OBJ::BOX)) {
						if (blocks[i][j+2]->getID() == static_cast<int>(OBJ::END)) { PlaySound(fxEnd); }
						blocks[i][j + 2]->setTexture(objects[static_cast<int>(OBJ::BOX)]);
						blocks[i][j + 2]->setID(static_cast<int>(OBJ::BOX));
					}
					blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::FLOOR)]);
					blocks[i][j]->setID(static_cast<int>(OBJ::FLOOR));
					blocks[i][j + 1]->setTexture(objects[static_cast<int>(OBJ::PJ)+3]);
					blocks[i][j + 1]->setID(static_cast<int>(OBJ::PJ));	
					right = false;
				}
				else {
					up = false;
					down = false;
					left = false;
					right = false;
				}
			}
		}
	}
}


bool level::getActive() {
	return active;
}
void level::setActive(bool _active) {
	active = _active;
}

