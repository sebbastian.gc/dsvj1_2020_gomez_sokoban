#ifndef LEVELZERO_H
#define LEVELZERO_H

#include "raylib.h"
#include "level.h"

namespace sokoban {

	class levelZero : public level {
	public:
		levelZero(bool _active);
		~levelZero();
		void levelDesign();
		void updatePoint();
		void isWin();
		void update();
	};
}
#endif
