#ifndef LEVELONE_H
#define LEVELONE_H

#include "raylib.h"
#include "level.h"

namespace sokoban {

	class levelOne : public level {		
	public:
		levelOne(bool _active);
		~levelOne();
		void levelDesign();
		void updatePoint();
		void isWin();
		void update();
	};
}
#endif
