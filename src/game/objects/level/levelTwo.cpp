#include "levelTwo.h"

using namespace sokoban;

levelTwo::levelTwo(bool _active) :level(_active) {

	levelDesign();
}
levelTwo::~levelTwo() {

}
void levelTwo::levelDesign() {
	for (short i = 0; i < size; i++) {
		for (short j = 0; j < size; j++) {
			if (i == 0 || j == 0 || j == size - 1 || i == size - 1 || i == size - 2 || j == size - 2|| i == 1 || j == 1 ) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::BLOCK)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::BLOCK));
			}
			else {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::FLOOR)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::FLOOR));
			}
			if (i == 4 && j == 2 || i == 3 && j == 2 || i == 3 && j == 4 || i == 4 && j == 4 || i == 5 && j == 5 ) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::BLOCK)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::BLOCK));
			}
			if (i == 1 && j == 3) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::FLOOR)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::FLOOR));
			}
			if (i == 1 && j == 2) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::PJ)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::PJ));
			}
			if (i == 5 && j == 2 || i == 6 && j == 5 || i == 2 && j == 3) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::BOX)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::BOX));

			}
			if (i == 6 && j == 1 || i == 5 && j == 1 || i == 4 && j == 1 ) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::END)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::END));
				blocks[i][j]->setColor(RED);
			}
		}
	}
}

void levelTwo::updatePoint() {
	for (short i = 0; i < size; i++) {
		for (short j = 0; j < size; j++) {
			if (i == 6 && j == 1 || i == 5 && j == 1 || i == 4 && j == 1) {
				if (blocks[i][j]->getID() == static_cast<int>(OBJ::FLOOR)) {
					blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::END)]);
					blocks[i][j]->setID(static_cast<int>(OBJ::END));
				}
			}
		}
	}
}

void levelTwo::isWin() {
	if (blocks[6][1]->getID() == static_cast<int>(OBJ::BOX) && blocks[5][1]->getID() == static_cast<int>(OBJ::BOX) && blocks[4][1]->getID() == static_cast<int>(OBJ::BOX)) {
		win = true;
	}
}

void levelTwo::update() {
	if (resetLevel) {
		levelDesign();
		resetLevel = false;
	}
	moveCharacter();
	updatePoint();
	isWin();
}
