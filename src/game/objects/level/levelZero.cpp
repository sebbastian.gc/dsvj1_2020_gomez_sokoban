#include "levelZero.h"

using namespace sokoban;

levelZero::levelZero(bool _active) :level(_active) {

	levelDesign();
}
levelZero::~levelZero() {

}
void levelZero::levelDesign() {
	for (short i = 0; i < size; i++) {
		for (short j = 0; j < size; j++) {
			if (i == 0 || j == 0 || j == size - 1 || i == size - 1||j == 4 || i == 4) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::BLOCK)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::BLOCK));
			}			
			else {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::FLOOR)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::FLOOR));
			}
			if (i == 4 && j == 4 || i == 2 && j == 4 || i == 5 && j == 4||i == 4 && j == 3 || i == 4 && j == 5 ) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::FLOOR)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::FLOOR));
			}
			if (i == 7 && j ==1 ) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::PJ)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::PJ));
			}
			if (i == 2 && j == 2 || i == 6 && j == 2 || i == 6 && j == 6) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::BOX)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::BOX));

			}
			if (i == 1 && j == 6 || i == 1 && j == 5 || i == 3 && j == 7) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::END)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::END));
				blocks[i][j]->setColor(RED);
			}
		}
	}
}

void levelZero::updatePoint() {
	for (short i = 0; i < size; i++) {
		for (short j = 0; j < size; j++) {
			if (i == 1 && j == 6 || i == 1 && j == 5 || i == 3 && j == 7) {
				if (blocks[i][j]->getID() == static_cast<int>(OBJ::FLOOR)) {
					blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::END)]);
					blocks[i][j]->setID(static_cast<int>(OBJ::END));
				}
			}
		}
	}	
}
void levelZero::isWin() {
	if (blocks[1][6]->getID() == static_cast<int>(OBJ::BOX) && blocks[1][5]->getID() == static_cast<int>(OBJ::BOX)&& blocks[3][7]->getID() == static_cast<int>(OBJ::BOX)) {
		win = true;
	}
}

void levelZero::update() {
	if (resetLevel) {
		levelDesign();
		resetLevel = false;
	}
	moveCharacter();
	updatePoint();
	isWin();
}
