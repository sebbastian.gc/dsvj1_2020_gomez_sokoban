#ifndef LEVELFOUR_H
#define LEVELFOUR_H

#include "raylib.h"
#include "level.h"

namespace sokoban {

	class levelFour : public level {
	public:
		levelFour(bool _active);
		~levelFour();
		void levelDesign();
		void updatePoint();
		void isWin();
		void update();
	};
}
#endif
