#ifndef LEVELTHREE_H
#define LEVELTHREE_H

#include "raylib.h"
#include "level.h"

namespace sokoban {

	class levelThree : public level {
	public:
		levelThree(bool _active);
		~levelThree();
		void levelDesign();
		void updatePoint();
		void isWin();
		void update();
	};
}
#endif
