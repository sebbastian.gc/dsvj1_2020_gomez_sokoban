#include "levelOne.h"

using namespace sokoban;

levelOne::levelOne(bool _active):level(_active){

	levelDesign();
}
levelOne::~levelOne() {

}
void levelOne::levelDesign() {
	for (short i = 0; i < size; i++) {
		for (short j = 0; j < size; j++) {
			if (i == 0 || j == 0 || j == size - 1 || i == size - 1 || i == size - 2||j == size - 2||i == 1 || j == 1) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::BLOCK)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::BLOCK));
			}
			else {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::FLOOR)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::FLOOR));
			}
			if (i == 2 && j == 3 || i == 5 && j == 4 || i == 6 && j == 6 || i == 5 && j == 6 || i == 4 && j == 6 || i == 6 && j == 2 || i == 5 && j == 2 ) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::BLOCK)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::BLOCK));
			}
			if (i == 3 && j == 1|| i == 4 && j == 1 || i == 1 && j == 2 || i == 1 && j == 3 || i == 1 && j == 4 || i == 1 && j == 1 || i == 2 && j == 1) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::FLOOR)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::FLOOR));
			}
			if (i == 6 && j == 3) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::PJ)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::PJ));
			}
			if (i == 4 && j == 3 || i == 4 && j == 4 || i == 3 && j == 4) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::BOX)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::BOX));

			}
			if (i == 5 && j == 5 || i == 3 && j == 5 || i == 3 && j == 3 ) {
				blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::END)]);
				blocks[i][j]->setID(static_cast<int>(OBJ::END));
				blocks[i][j]->setColor(RED);
			}
		}
	}
}

void levelOne::updatePoint() {
	for (short i = 0; i < size; i++) {
		for (short j = 0; j < size; j++) {
			if (i == 5 && j == 5 || i == 3 && j == 5 || i == 3 && j == 3) {
				if (blocks[i][j]->getID() == static_cast<int>(OBJ::FLOOR)) {
					blocks[i][j]->setTexture(objects[static_cast<int>(OBJ::END)]);
					blocks[i][j]->setID(static_cast<int>(OBJ::END));
				}
			}
		}
	}
}

void levelOne::isWin() {
	if ( blocks[5][5]->getID() == static_cast<int>(OBJ::BOX)&& blocks[3][5]->getID() == static_cast<int>(OBJ::BOX)&& blocks[3][3]->getID() == static_cast<int>(OBJ::BOX)) {
		win = true;
	}
}

void levelOne::update() {
	if (resetLevel) {
		levelDesign();
		resetLevel = false;
	}
	moveCharacter();
	updatePoint();
	isWin();
}
