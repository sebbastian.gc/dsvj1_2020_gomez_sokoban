#ifndef LEVEL_H
#define LEVEL_H

#include "raylib.h"
#include "game/globales.h"
#include "game/objects/block/block.h"

namespace sokoban {

	class level {
	protected:
		block* blocks[size][size];
		Texture2D objects[sizeObj];
		bool active;
		Sound fxEnd;
	public:
		level(bool _active);
		~level();
		void init(bool _active);
		virtual void update() = 0;
		void draw();
		void spawnBlocks();
		virtual void updatePoint()=0;
		void moveCharacter();
		virtual void isWin()=0;
		virtual void levelDesign() = 0;
		bool getActive();
		void setActive(bool _active);
	};
}
#endif