#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"
#include "game/globales.h"
#include "game/objects/button/button.h"

namespace sokoban {
	namespace gameplay {

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif
