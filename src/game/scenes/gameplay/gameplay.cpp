#include "gameplay.h"
#include "game/input/input.h"
#include "game/objects/level/level.h"
#include "game/objects/level/levelZero.h"
#include "game/objects/level/levelOne.h"
#include "game/objects/level/levelTwo.h"
#include "game/objects/level/levelThree.h"
#include "game/objects/level/levelFour.h"

bool sokoban::resetLevel;
bool sokoban::gameOver;
bool sokoban::win;

namespace sokoban {
	namespace gameplay {

		level* levels[sizeLevels];
		button* back;
		button* reset;

		Sound     fxButton;
		Music     gameplayMusic;

		Texture2D backTexture;
		Texture2D resetTexture;
		Texture2D backgroundTexture;
		Texture2D winTexture;
		Texture2D pauseTexture;
		Texture2D gameOverTexture;

		float time;
		short timeToLose;
		float volume;

		void static reboot() {
			StopMusicStream(gameplayMusic);
			currentScene = SCENES::MENU;
			initscenes = true;
			input::pause = false;
			deinit();
		}
		void static buttonBack() {
			back->update();
			if (back->getButtonAction()) {
				reboot();
			}
		}	
		void static buttonReset() {
			reset->update();
			if (reset->getButtonAction()) {
				gameOver = false;
				win = false;
				input::pause = false;
				resetLevel = true;
				time = 0.0f;
			}
		}
		void static timer() {
			time++;
			if (static_cast<int>(time / frameRate) >= timeToLose) {
				gameOver = true;
			}			
		}

		void static music() {
			UpdateMusicStream(gameplayMusic);
			if (input::mute) { volume = 0; }
			else volume = 0.2f;
			SetMusicVolume(gameplayMusic, volume);
			PlayMusicStream(gameplayMusic);

		}

		void static drawWin() {
			if (win) {
				DrawTexture(winTexture, 0, 0, RAYWHITE);
				back->draw();
				reset->draw();
			}
		}
		void static drawGameOver() {
			if (gameOver) {
				DrawTexture(gameOverTexture, 0, 0, RAYWHITE);
				back->draw();
				reset->draw();
			}
		}
		void static drawPause() {
			if (input::pause) {
				back->draw();
				reset->draw();
				DrawTexture(pauseTexture, 0, 0, RAYWHITE);
			}
		}
		
		void init() {			
			backTexture = LoadTexture("res/assets/button_back.png");
			resetTexture = LoadTexture("res/assets/button_reset.png");
			backgroundTexture= LoadTexture("res/assets/fondo.png");
			winTexture = LoadTexture("res/assets/win.png");
			pauseTexture= LoadTexture("res/assets/pause.png");
			gameOverTexture = LoadTexture("res/assets/gameOver.png");

			fxButton = LoadSound("res/fx/button.mp3");
			gameplayMusic= LoadMusicStream("res/fx/gameplay.mp3");

			levels[0] = new levelZero(true);
			levels[1] = new levelOne(true);
			levels[2] = new levelTwo(true);
			levels[3] = new levelThree(true);
			levels[4] = new levelFour(true);			

			back = new button(backTexture, static_cast<float>(screenWidth / 2.5f), 15.0f, fxButton);
			reset = new button(resetTexture, static_cast<float>(screenWidth / 100.0f), 15.0f, fxButton);
			resetLevel=false;
			gameOver = false;
			win = false;
			time = 0.0f;
			timeToLose = 180;
			volume = 0.2f;
			initscenes = false;
		}

		void update() {
			if (levels[selectLevel] != NULL) {
				music();
				if (!win && !input::pause && !gameOver) {
					levels[selectLevel]->update();
					timer();
				}
				else {
					buttonReset();
					buttonBack();
				}							
			}			
		}

		void draw() {
			if (levels[selectLevel] != NULL) {
				DrawTexture(backgroundTexture, 0, 0, RAYWHITE);	
				DrawText(TextFormat("%03i", static_cast<int>(time / frameRate)), static_cast<int>(screenWidth / 5.5f), static_cast<int>(screenHeight * 0.006f), 30, WHITE);

				levels[selectLevel]->draw();
				drawPause();
				drawGameOver();
				drawWin();
			}			
		}
		void deinit() {
			delete back;
			back = NULL;
			delete reset;
			reset= NULL;

			for (short i = 0; i < sizeLevels; i++){
				delete levels[i];
				levels[i] = NULL;
			}
			
			UnloadMusicStream(gameplayMusic);
			UnloadSound(fxButton);
			UnloadTexture(backgroundTexture);
			UnloadTexture(winTexture);
			UnloadTexture(pauseTexture);
			UnloadTexture(gameOverTexture);
			
		}			
	}
}