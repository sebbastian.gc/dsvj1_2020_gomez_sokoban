#ifndef MENU_H
#define MENU_H

#include "raylib.h"
#include "game/objects/button/button.h"

using namespace sokoban;

namespace sokoban {

	namespace menu {

		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif