#include "menu.h"
#include "game/input/input.h"

#include "game/globales.h"

using namespace sokoban;

SCREENS sokoban::currentScreen;
 short sokoban::selectLevel;

namespace sokoban {
	namespace menu {

		Texture2D screenMenuTexture;
		Texture2D screenCreditsTexture;
		Texture2D screenInstTexture;

		Texture2D playTexture;
		Texture2D creditsTexture;
		Texture2D exitTexture;
		Texture2D backTexture;
		Texture2D startTexture;
		Texture2D fiveTexture;
		Texture2D oneTexture;
		Texture2D twoTexture;
		Texture2D threeTexture;
		Texture2D fourTexture;
		Music     menuMusic;
		Sound     fxButton;

		button* play;
		button* credits;
		button* exit;
		button* back;
		button* start;
		button* one;
		button* two;
		button* three;
		button* four;
		button* five;

		float   volume;
		void static playButton() {
			if (play != NULL) {
				play->update();
				if (play->getButtonAction()) {
					currentScreen = SCREENS::INSTRUCTIONS;
				}
			}
		}
		void static startButton() {
			start->update();
			if (start->getButtonAction()) {
				currentScene = SCENES::GAMEPLAY;
				initscenes = true;
				deinit();
			}
		}
		void static levelButtons() {
			one->update();
			two->update();
			three->update();
			four->update();
			five->update();
			if (one->getButtonAction()) {
				selectLevel = 0;
			}
			if (two->getButtonAction()) {
				selectLevel = 1;
			}
			if (three->getButtonAction()) {
				selectLevel = 2;
			}				
			if (four->getButtonAction()) {
				selectLevel = 3;
			}
			if (five->getButtonAction()) {
				selectLevel = 4;
			}
		}

		void static creditsButton() {
			if (credits != NULL) {
				credits->update();
				if (credits->getButtonAction()) {
					currentScreen = SCREENS::CREDITS;
				}
			}

		}
		void static exitButton() {
			if (exit != NULL) {
				exit->update();
				if (exit->getButtonAction()) {
					CloseWindow();
				}
			}
		}
		void static backButton() {
			back->update();
			if (back->getButtonAction()) {
				currentScreen = SCREENS::MENU;
			}
		}
		void static drawMenu() {
			DrawTexture(screenMenuTexture, 0, 0, WHITE);
			play->draw();
			credits->draw();
			exit->draw();
		}

		void static music() {
			UpdateMusicStream(menuMusic);
			if (input::mute) { volume = 0; }
			else volume = 0.2f;
			SetMusicVolume(menuMusic, volume);
			PlayMusicStream(menuMusic);

		}
		
		void init() {
			currentScreen = SCREENS::MENU;
			currentScene = SCENES::MENU;
			screenMenuTexture = LoadTexture("res/assets/menu.png");
			screenCreditsTexture = LoadTexture("res/assets/screen_credits.png");
			screenInstTexture = LoadTexture("res/assets/start.png");

			playTexture = LoadTexture("res/assets/button_play.png");
			creditsTexture = LoadTexture("res/assets/button_credits.png");
			exitTexture = LoadTexture("res/assets/button_exit.png");
			backTexture = LoadTexture("res/assets/button_back.png");
			startTexture = LoadTexture("res/assets/button_start.png");			 
			 oneTexture = LoadTexture("res/assets/1.png");
			 twoTexture = LoadTexture("res/assets/2.png");
			 threeTexture = LoadTexture("res/assets/3.png");
			 fourTexture = LoadTexture("res/assets/4.png");
			 fiveTexture = LoadTexture("res/assets/5.png");

			menuMusic = LoadMusicStream("res/fx/menu.mp3");
			fxButton = LoadSound("res/fx/button.mp3");

			play = new button(playTexture, -75.0f, fxButton);
			credits = new button(creditsTexture, 0.0f, fxButton);
			exit = new button(exitTexture, 75.0f, fxButton);
			start = new button(startTexture, static_cast<float>(screenHeight / 2.5f), fxButton);
			back = new button(backTexture, static_cast<float>(screenWidth / 2.5f), 15.0f, fxButton);
			one=new button(oneTexture, -200.0f, 380.0f, fxButton);
			two=new button(twoTexture, -100.0f, 380.0f, fxButton);
			three=new button(threeTexture, 0.0f, 380.0f, fxButton);
			four=new button(fourTexture, static_cast<float>(screenWidth / 4.5f), 380.0f, fxButton);
			five=new button(fiveTexture, static_cast<float>(screenWidth / 2.5f), 380.0f, fxButton);
			initscenes = false;
			selectLevel = 0;
			volume = 0.2f;			
		}
		void update() {
			if (play != NULL) {
				music();
				switch (currentScreen) {
				case SCREENS::MENU:
					playButton();
					creditsButton();
					exitButton();
					break;
				case SCREENS::CREDITS:
					backButton();
					break;
				case SCREENS::INSTRUCTIONS:
					levelButtons();
					startButton();
					break;
				}
			}
			
		}
		void draw() {
			if (play != NULL) {
				switch (currentScreen) {
				case SCREENS::MENU:
					drawMenu();
					break;
				case SCREENS::CREDITS:
					DrawTexture(screenCreditsTexture, 0, 0, WHITE);
					back->draw();
					break;
				case SCREENS::INSTRUCTIONS:
					DrawTexture(screenInstTexture, 0, 0, WHITE);
					start->draw();
					one->draw();
					two->draw();
					three->draw();
					four->draw();
					five->draw();
					break;
				}
			}			
		}
		void deinit() {
			UnloadTexture(screenMenuTexture);
			UnloadTexture(screenCreditsTexture);
			UnloadTexture(screenInstTexture);
			UnloadTexture(oneTexture);
			UnloadTexture(twoTexture);
			UnloadTexture(threeTexture);
			UnloadTexture(fourTexture);
			UnloadTexture(fiveTexture);
			UnloadMusicStream(menuMusic);

			delete back;
			delete credits;
			delete exit;
			delete play;
			delete start;
			delete one;
			delete two;
			delete three;
			delete four;
			delete five;

			one = NULL;
			two = NULL;
			three = NULL;
			four = NULL;
			five = NULL;
			back = NULL;
			credits = NULL;
			exit = NULL;
			play = NULL;
			start = NULL;

			UnloadSound(fxButton);			
		}
	}
}