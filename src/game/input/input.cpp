#include "input.h"
#include "game/globales.h"

using namespace sokoban;

bool input::mute;
bool input::up;
bool input::down;
bool input::left;
bool input::right;
bool input::pause;

namespace sokoban {

	namespace input {

		void init() {
			mute = false;
			up = false;
			down= false;
			left = false;
			right = false;
			pause = false;
		}
		void update() {				
			if (IsKeyPressed(KEY_UP)) { up = true; }
			else if (IsKeyPressed(KEY_DOWN)) { down = true; }
			else if (IsKeyPressed(KEY_LEFT)) { left = true; }
			else if (IsKeyPressed(KEY_RIGHT)) { right = true; }
			if (IsKeyPressed(KEY_SPACE)) { !pause ? pause = true : pause = false; }	
			if (IsKeyPressed(KEY_Q)) { !mute ? mute = true : mute = false; }
		}
	}
}