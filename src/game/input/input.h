#ifndef INPUT_H
#define INPUT_H

#include "raylib.h"

namespace sokoban {

	namespace input {

		extern bool mute;
		extern bool pause;
		extern bool up;
		extern bool down;
		extern bool left;
		extern bool right;

		void init();
		void update();
	}
}
#endif
