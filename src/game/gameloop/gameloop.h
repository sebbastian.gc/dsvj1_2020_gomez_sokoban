#ifndef GAMELOOP_H
#define GAMELOOP_H

#include "raylib.h"

namespace sokoban {

	namespace gameLoop {
		void run();
	}

}
#endif
