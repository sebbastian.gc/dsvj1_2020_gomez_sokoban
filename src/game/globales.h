
#ifndef GLOBAL_H
#define GLOBAL_H

#include <iostream>
#include <stdlib.h>
#include <time.h>

namespace sokoban {

	extern int screenWidth;
	extern int screenHeight;
	extern bool initscenes;
	extern bool resetLevel;
	extern bool gameOver;
	extern bool win;
	const  float frameRate = 60.0f;
	const short size = 9;
	const short sizeObj = 8;
	const short sizeLevels = 5;
	extern short selectLevel;

	enum class SCENES { MENU, GAMEPLAY };
	enum class SCREENS { MENU, CREDITS,  GAMEPLAY, INSTRUCTIONS	};
	enum class OBJ {BLOCK,FLOOR,BOX,END,PJ};

	extern SCENES currentScene;
	extern SCREENS currentScreen;
	
	
}
#endif
