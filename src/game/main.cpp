#include "gameLoop/gameLoop.h"

using namespace sokoban;

int main() {
    gameLoop::run();

    return 0;
}